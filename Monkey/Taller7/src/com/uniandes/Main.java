package com.uniandes;

import org.apache.commons.net.telnet.TelnetClient;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;
import java.util.Arrays;


public class Main {

    public static final String ADB_ROOT = "C:/Users/Omar/AppData/Local/Android/sdk/platform-tools/";
    public static final String TELNET_TOKEN = "8uK5GKMmm4HBYHYS";
    public static final Integer EMULATOR_PORT = 5554;

    public static final String ADB_INPUT_TAP = ADB_ROOT + "adb shell input tap ";
    public static final String ADB_INPUT_TEXT = ADB_ROOT + "adb shell input text ";
    public static final String ADB_INPUT_SWIPE = ADB_ROOT + "adb shell input swipe ";
    public static final String ADB_INPUT_KEYEVENT = ADB_ROOT + "adb shell input keyevent ";
    public static final String ADB_NETWORK_SPEED = "network speed ";

    public static final String ADB_INSTALL = ADB_ROOT + "adb install ";
    public static final String ADB_UNINSTALL = ADB_ROOT + "adb uninstall ";
    public static final String ADB_OPEN_1 = ADB_ROOT + "adb shell monkey -p ";
    public static final String ADB_OPEN_2 = " -c android.intent.category.LAUNCHER 1";
    public static TelnetClient telnet = null;

    private static BufferedWriter connectToTelnet() throws IOException {
        //Runtime rt = Runtime.getRuntime();
        //Process telnet = rt.exec("telnet localhost "+EMULATOR_PORT);
        //return new BufferedWriter(new OutputStreamWriter(telnet.getOutputStream()));
        telnet = new TelnetClient();
        telnet.connect("localhost", EMULATOR_PORT);
        return new BufferedWriter(new OutputStreamWriter(telnet.getOutputStream()));
    }

    public static void rotate() throws IOException {
        BufferedWriter out = connectToTelnet();
        out.write("auth "+TELNET_TOKEN+"\n");
        out.write("rotate\n");
        out.write("quit\n");
        out.flush();
    }

    public static void velocity() throws IOException {
        BufferedWriter out = connectToTelnet();
        out.write("auth "+TELNET_TOKEN+"\n");
        out.write( ADB_NETWORK_SPEED + getVelocity() +"\n");
        out.write("quit\n");
        out.flush();
    }

    public static void sensors() throws IOException {
        BufferedWriter out = connectToTelnet();
        out.write("auth "+TELNET_TOKEN+"\n");
        out.write("sensor set acceleration 2.23517e-07:9.77631:0.812348\n");
        out.write("quit\n");
        out.flush();
    }

    public static void main(String[] args) {
        Runtime rt = Runtime.getRuntime();
        try {
            String input = "";
            Scanner scanner = new Scanner(System.in);
            Scanner scanner1 = new Scanner(System.in);
            do {
                System.out.println("--- Inicio Programa ---");
                System.out.println("Introduzca la ruta del apk y el paquete de la app, o solo el nombre del paquete si la aplicación ya se encuentra instalada [c:\\app.apk, app.paquete.com]:");
                String apkPathPack = scanner.nextLine().trim();

                while(apkPathPack.isEmpty()){
                    System.out.println("Introduzca la ruta del apk y el paquete de la app, o solo el nombre del paquete si la aplicación ya se encuentra instalada [c:\\app.apk, app.paquete.com]:");
                    apkPathPack = scanner.nextLine().trim();
                }

                String apkPath = "";
                String pack = "";
                String[] apkPathPackArray = apkPathPack.split(",");
                if(apkPathPackArray.length > 1) {
                    apkPath = apkPathPackArray[0].trim();
                    pack = apkPathPackArray[1].trim();
                }else{
                    pack = apkPathPackArray[0].trim();
                }

                System.out.println("Introduzca el número de eventos a ejecutar:");

                int events = Integer.parseInt(scanner.nextLine().trim());

                while(events <= 0){
                    System.out.println("Introduzca el número de eventos a ejecutar:");
                    events = Integer.parseInt(scanner.nextLine().trim());
                }

                System.out.println("Introduzca los comandos a ejecutar [tap,text,swipe,keyevent,rotate,speed,sensor-acceleration]:");
                String commands = scanner.nextLine().trim();

                Process p;
                int count = 0;
                if(apkPathPackArray.length > 1) {
                    System.out.println("Instalando aplicación...");

                    p = rt.exec(ADB_UNINSTALL + pack);
                    while (p.isAlive()) {
                        if (count % 100000 == 0) {
                            System.out.print(".");
                        }
                        count++;
                    }

                    p = rt.exec(ADB_INSTALL + "\"" + apkPath + "\"");
                    count = 0;
                    while(p.isAlive()) {
                        if (count % 1000000 == 0) {
                            System.out.print(".");
                        }
                        count++;
                    }

                    System.out.println("\nInstalación exitosa.");
                }

                System.out.println("Abriendo aplicación...");
                p = rt.exec(ADB_OPEN_1 + pack + ADB_OPEN_2);
                count = 0;
                while(p.isAlive()) {
                    if (count % 100000 == 0) {
                        System.out.print(".");
                    }
                    count++;
                }

                System.out.println("\n--- Ejecución Monkey ---");

                Random random = new Random(12345);

                String[] commandsArray = commands.split(",");

                for (int i = 0; i < commandsArray.length; i++) {
                    commandsArray[i] = commandsArray[i].trim();
                }

                int i = 0;
                while (i < events) {
                    int x = random.nextInt(1080);
                    int y = random.nextInt(1920);
                    int x1 = random.nextInt(1080);
                    int y1 = random.nextInt(1920);
                    int telnetEvents = random.nextInt(4);

                    if (Arrays.asList(commandsArray).contains("rotate")) {
                        if (telnetEvents == 1) {
                            Main.rotate();
                        }
                    }

                    if (Arrays.asList(commandsArray).contains("speed")) {
                        if (telnetEvents == 2) {
                            Main.velocity();
                        }
                    }

                    if (Arrays.asList(commandsArray).contains("sensor-acceleration")) {
                        if (telnetEvents == 3) {
                            Main.sensors();
                        }
                    }

                    if (Arrays.asList(commandsArray).contains("tap")) {
                        rt.exec(ADB_INPUT_TAP + x + " " + y);
                    }

                    if (Arrays.asList(commandsArray).contains("text")) {
                        rt.exec(ADB_INPUT_TEXT + new RandomString(5, ThreadLocalRandom.current()));
                    }

                    if (Arrays.asList(commandsArray).contains("swipe")) {
                        rt.exec(ADB_INPUT_SWIPE + x + " " + y + " " + x1 + " " + y1);
                    }

                    if (Arrays.asList(commandsArray).contains("keyevent")) {
                        rt.exec(ADB_INPUT_KEYEVENT + GetKey());
                    }

                    i++;
                    Thread.sleep(1000);
                }


                System.out.println("\n¿Desea realizar otra nueva ejecución?(S/N)");
                input = scanner1.nextLine();
            } while (input.toLowerCase().equals("s"));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static int GetKey() {
        int[] intArray = new int[]{55, 67, 62, 77, 70, 232, 255, 24, 25, 164};

        Random random = new Random();
        int key = random.nextInt(10);

        return intArray[key];
    }

    private static String getVelocity() {
        String[] intArray = new String[]{"gsm", "hscsd", "gprs", "edge", "umts", "hsdpa", "lte", "evdo", "full" };

        Random random = new Random();
        int key = random.nextInt(9);

        return intArray[key];
    }
}