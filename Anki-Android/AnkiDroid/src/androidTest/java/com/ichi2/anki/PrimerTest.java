package com.ichi2.anki;


import android.support.test.espresso.DataInteraction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.hamcrest.core.IsInstanceOf;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.*;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.anything;
import static org.hamcrest.Matchers.is;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class PrimerTest {

    @Rule
    public ActivityTestRule<IntentHandler> mActivityTestRule = new ActivityTestRule<>(IntentHandler.class);

    @Test
    public void primerTest() {
        ViewInteraction viewInteraction = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction.perform(click());

        ViewInteraction viewInteraction2 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction2.perform(click());

        ViewInteraction viewInteraction3 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction3.perform(click());

        ViewInteraction floatingActionButton = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                2),
                        isDisplayed()));
        floatingActionButton.perform(click());

        ViewInteraction appCompatSpinner = onView(
                allOf(withId(R.id.note_type_spinner),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorLayout),
                                        0),
                                1)));
        appCompatSpinner.perform(scrollTo(), click());

        DataInteraction appCompatCheckedTextView = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(1);
        appCompatCheckedTextView.perform(click());

        ViewInteraction appCompatSpinner2 = onView(
                allOf(withId(R.id.note_deck_spinner),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorLayout),
                                        1),
                                1)));
        appCompatSpinner2.perform(scrollTo(), click());

        DataInteraction appCompatCheckedTextView2 = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(0);
        appCompatCheckedTextView2.perform(click());

        ViewInteraction fieldEditText = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        1),
                                0)));
        fieldEditText.perform(scrollTo(), replaceText("pru"), closeSoftKeyboard());

        ViewInteraction fieldEditText2 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        3),
                                0)));
        fieldEditText2.perform(scrollTo(), replaceText("bà"), closeSoftKeyboard());

        ViewInteraction actionMenuItemView = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        3),
                                0),
                        isDisplayed()));
        actionMenuItemView.perform(click());

        pressBack();


        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.files),
                        childAtPosition(
                                withId(R.id.pull_to_sync_wrapper),
                                0)));
        recyclerView.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction linearLayout = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        childAtPosition(
                                allOf(withId(R.id.answer_options_layout),
                                        childAtPosition(
                                                withId(R.id.bottom_area_layout),
                                                1)),
                                0),
                        isDisplayed()));
        linearLayout.perform(click());

        ViewInteraction appCompatImageButton = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.front_frame),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton.perform(click());

        ViewInteraction navigationMenuItemView = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        1),
                        isDisplayed()));
        navigationMenuItemView.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction recyclerView2 = onView(
                allOf(withId(R.id.files),
                        childAtPosition(
                                withId(R.id.pull_to_sync_wrapper),
                                0)));
        recyclerView2.perform(actionOnItemAtPosition(0, click()));

        ViewInteraction view = onView(
                allOf(withContentDescription("pru"),
                        childAtPosition(
                                allOf(withContentDescription("AnkidDroid Flashcard"),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.webkit.WebView.class),
                                                0)),
                                0),
                        isDisplayed()));
        view.check(matches(isDisplayed()));

        ViewInteraction appCompatImageButton2 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withId(R.id.front_frame),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton2.perform(click());

        ViewInteraction navigationMenuItemView2 = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.design_navigation_view),
                                childAtPosition(
                                        withId(R.id.navdrawer_items_container),
                                        0)),
                        1),
                        isDisplayed()));
        navigationMenuItemView2.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction viewInteraction4 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction4.perform(click());

        ViewInteraction floatingActionButton2 = onView(
                allOf(withId(R.id.add_deck_action), withContentDescription("Create deck"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                0),
                        isDisplayed()));
        floatingActionButton2.perform(click());

        ViewInteraction editText = onView(
                childAtPosition(
                        childAtPosition(
                                withId(R.id.customViewFrame),
                                0),
                        0));
        editText.perform(scrollTo(), click());

        ViewInteraction editText2 = onView(
                childAtPosition(
                        childAtPosition(
                                withId(R.id.customViewFrame),
                                0),
                        0));
        editText2.perform(scrollTo(), replaceText("nuev"), closeSoftKeyboard());

        ViewInteraction mDButton = onView(
                allOf(withId(R.id.buttonDefaultPositive), withText("OK"),
                        childAtPosition(
                                allOf(withId(R.id.root),
                                        childAtPosition(
                                                withId(android.R.id.content),
                                                0)),
                                4),
                        isDisplayed()));
        mDButton.perform(click());

        ViewInteraction recyclerView3 = onView(
                allOf(withId(R.id.files),
                        childAtPosition(
                                withId(R.id.pull_to_sync_wrapper),
                                0)));
        recyclerView3.perform(actionOnItemAtPosition(1, click()));

        ViewInteraction viewInteraction5 = onView(
                allOf(withId(R.id.fab_expand_menu_button), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                3),
                        isDisplayed()));
        viewInteraction5.perform(click());

        ViewInteraction floatingActionButton3 = onView(
                allOf(withId(R.id.add_note_action), withContentDescription("Add"),
                        childAtPosition(
                                allOf(withId(R.id.add_content_menu),
                                        childAtPosition(
                                                withId(R.id.root_layout),
                                                2)),
                                2),
                        isDisplayed()));
        floatingActionButton3.perform(click());

        ViewInteraction appCompatSpinner3 = onView(
                allOf(withId(R.id.note_type_spinner),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorLayout),
                                        0),
                                1)));
        appCompatSpinner3.perform(scrollTo(), click());

        DataInteraction appCompatCheckedTextView3 = onData(anything())
                .inAdapterView(childAtPosition(
                        withClassName(is("android.widget.PopupWindow$PopupBackgroundView")),
                        0))
                .atPosition(0);
        appCompatCheckedTextView3.perform(click());

        ViewInteraction fieldEditText3 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Front"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        1),
                                0)));
        fieldEditText3.perform(scrollTo(), replaceText("card"), closeSoftKeyboard());

        ViewInteraction fieldEditText4 = onView(
                allOf(withId(R.id.id_note_editText), withContentDescription("Back"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.CardEditorEditFieldsLayout),
                                        3),
                                0)));
        fieldEditText4.perform(scrollTo(), replaceText("era"), closeSoftKeyboard());

        ViewInteraction actionMenuItemView2 = onView(
                allOf(withId(R.id.action_save), withContentDescription("Save"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.toolbar),
                                        3),
                                0),
                        isDisplayed()));
        actionMenuItemView2.perform(click());

        ViewInteraction appCompatImageButton3 = onView(
                allOf(withContentDescription("Navigate up"),
                        childAtPosition(
                                allOf(withId(R.id.toolbar),
                                        childAtPosition(
                                                withClassName(is("android.widget.LinearLayout")),
                                                0)),
                                0),
                        isDisplayed()));
        appCompatImageButton3.perform(click());

        ViewInteraction recyclerView4 = onView(
                allOf(withId(R.id.files),
                        childAtPosition(
                                withId(R.id.pull_to_sync_wrapper),
                                0)));
        recyclerView4.perform(actionOnItemAtPosition(1, click()));

        ViewInteraction view2 = onView(
                allOf(withContentDescription("card"),
                        childAtPosition(
                                allOf(withContentDescription("AnkidDroid Flashcard"),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.webkit.WebView.class),
                                                0)),
                                0),
                        isDisplayed()));
        view2.check(matches(isDisplayed()));

        ViewInteraction frameLayout = onView(
                allOf(withId(R.id.touch_layer),
                        childAtPosition(
                                allOf(withId(R.id.flashcard_frame),
                                        childAtPosition(
                                                withId(R.id.front_frame),
                                                2)),
                                1),
                        isDisplayed()));
        frameLayout.perform(click());

        ViewInteraction linearLayout2 = onView(
                allOf(withId(R.id.flashcard_layout_flip),
                        childAtPosition(
                                allOf(withId(R.id.answer_options_layout),
                                        childAtPosition(
                                                withId(R.id.bottom_area_layout),
                                                1)),
                                0),
                        isDisplayed()));
        linearLayout2.perform(click());

        ViewInteraction view3 = onView(
                allOf(withContentDescription("era"),
                        childAtPosition(
                                allOf(withContentDescription("AnkidDroid Flashcard"),
                                        childAtPosition(
                                                IsInstanceOf.<View>instanceOf(android.webkit.WebView.class),
                                                0)),
                                2),
                        isDisplayed()));
        view3.check(matches(isDisplayed()));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
